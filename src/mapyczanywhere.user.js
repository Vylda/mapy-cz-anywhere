// ==UserScript==
// @name        Mapy.cz anywhere
// @version     2.0.0
// @author      Vilém Lipold
// @namespace   http://www.gcgpx.com/mapyczanywhere
// @description U kešky a waypointu se bude objevovat odkaz na turistické Mapy.cz a původní mapy Geocaching (otevře se v novém okně)
// @license     MIT License; http://www.opensource.org/licenses/mit-license.php
// @copyright   2017-24, Vilém Lipold (http://www.gcgpx.cz/)
// @updateURL   http://www.gcgpx.cz/mapyczanywhere/mapyczanywhere.meta.js
// @downloadURL http://www.gcgpx.cz/mapyczanywhere/mapyczanywhere.user.js
// @icon 		    http://www.gcgpx.cz/mapyczanywhere/mapyczanywhere.png
// @icon64 		  http://www.gcgpx.cz/mapyczanywhere/mapyczanywhere.png
// @match       https://www.geocaching.com/geocache/*
// @match       https://www.geocaching.com/seek/*
// @homepage 	  http://www.gcgpx.cz/mapyczanywhere
// @run-at	    document-end
// @grant       none
// ==/UserScript==

(function () {
	const w = window.wrappedJSObject || window;
	const fnc = () => {
		w.removeEventListener("load", fnc);

		const mapLinks = document.querySelector("#ctl00_ContentBody_MapLinks_MapLinks ul");
		if (!mapLinks) {
			return;
		}

		const firstAnchor = mapLinks.querySelector("li a");
		if (!firstAnchor) { return; }

		let { lat, lng } = w;

		if (!lat || !lng) {
			let url = firstAnchor.href;
			let latArray = url.match(/lat=([\d.]+)/);
			let lonArray = url.match(/lng=([\d.]+)/);

			if (latArray[1] && lonArray[1]) {
				lat = parseFloat(latArray[1]);
				lng = parseFloat(lonArray[1]);
			}
		}

		if (!!lat && !!lng) {
			const li1 = document.createElement("li");
			const anchorMapy = document.createElement("a");
			anchorMapy.href = `https://mapy.cz/turisticka?x=${lng}&y=${lat}&z=17&source=coor&id=${lng}%2C${lat}&q=${lat}%2${lng < 100 ? "0" : ""}${lng}`;
			anchorMapy.rel = "noopener noreferrer"
			anchorMapy.target = "_blank";
			anchorMapy.textContent = "Turistické Mapy.cz";
			li1.appendChild(anchorMapy);

			const li2 = document.createElement("li");
			const anchorOldGCMap = document.createElement("a");
			anchorOldGCMap.href = `https://www.geocaching.com/map/#?ll=${lat},${lng}&z=15`;
			anchorOldGCMap.rel = "noopener noreferrer"
			anchorOldGCMap.target = "_blank";
			anchorOldGCMap.textContent = "původní GC mapa";
			li2.appendChild(anchorOldGCMap);

			const gcmap = firstAnchor.parentNode.cloneNode(true);
			gcmap.style.display = "none";

			const fragment = document.createDocumentFragment()
			fragment.appendChild(gcmap);
			fragment.appendChild(li1);
			fragment.appendChild(li2);
			mapLinks.insertBefore(fragment, firstAnchor.parentNode);
		}
	}

	w.addEventListener("load", fnc);
}());
