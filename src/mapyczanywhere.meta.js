// ==UserScript==
// @name        Mapy.cz anywhere
// @version     2.0.0
// @author      Vilém Lipold
// @namespace   http://www.gcgpx.com/mapyczanywhere
// @description U kešky a waypointu se bude objevovat odkaz na turistické Mapy.cz a původní mapy Geocaching (otevře se v novém okně)
// @license     MIT License; http://www.opensource.org/licenses/mit-license.php
// @copyright   2017-24, Vilém Lipold (http://www.gcgpx.cz/)
// @updateURL   http://www.gcgpx.cz/mapyczanywhere/mapyczanywhere.meta.js
// @downloadURL http://www.gcgpx.cz/mapyczanywhere/mapyczanywhere.user.js
// @icon 		    http://www.gcgpx.cz/mapyczanywhere/mapyczanywhere.png
// @icon64 		  http://www.gcgpx.cz/mapyczanywhere/mapyczanywhere.png
// @match       https://www.geocaching.com/geocache/*
// @match       https://www.geocaching.com/seek/*
// @homepage 	  http://www.gcgpx.cz/mapyczanywhere
// @run-at	    document-end
// @grant       none
// ==/UserScript==
